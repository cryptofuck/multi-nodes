# Multi-nodes script

A script for simultaneously raising Minima *n*-node test network.

## Getting started

You need to have a VPS virtual server. You can rent it from any hoster. You can also install a container stack on your home computer.

To start, run the following command in the console
```
wget -qO minima_nodes.sh https://gitlab.com/cryptofuck/multi-nodes/-/raw/main/minima_nodes.sh && chmod +x minima_nodes.sh && sudo /bin/bash minima_nodes.sh
```

Select 'N' or 'A', depending on your wishes. 
* 'N' - will install Docker and any dependencies you need to run, and then start the stack.
* 'A' - will add nth number of containers to the already running stack.

During the installation, you will be presented with the Nano text editor.
* Write each '**INCENTIVE ID**' on a new line. Be careful and check everything carefully.
* Save the document under the suggested name **Ctrl+O**
* Close the document **Ctrl+X**

Enjoy the moment, in a few minutes everything will be ready.

## Monitoring

Select the 'M' option.
You will see information for each container:
* INCENTIVE ID
* LAST PING
* DAILY REWARDS

You can also use other Docker commands of your choice.

## Links 
https://minima.global/ - Official page of the Minima project.

https://incentivecash.minima.global/ - Registering for the test network.

