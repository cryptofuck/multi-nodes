#!/bin/bash

# welcome
echo "Введи нужный вариант:
N - Создать нове контейнеры 
A - Добавить к существующим контейнекам
M - Мониторинг запущеных контейнеров\n"

install() {
  # install dependencies
  sudo apt-get -y update
  sudo apt-get -y install \
      apt-transport-https \
      ca-certificates \
      curl \
      gnupg \
      lsb-release

  # install docker
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
  echo \
    "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
  sudo apt-get -y update
  sudo apt-get -y install docker-ce docker-ce-cli containerd.io

  # docker-compose permission
  sudo chmod 666 /var/run/docker.sock

  # install more dependencies
  sudo apt-get -y install jq

  # user part of the docker group
  sudo usermod -aG docker $USER
}

# running
run() {
  i=0
  while [ $i -lt $(cat $FILE | wc -l) ]
  do
    docker run -dit --restart on-failure --name minima_node_$PORT2 -v $HOME/.minima/minima_$PORT2/:/minima/.minima -p $PORT1:9001 -p $PORT2:9002 secord/minima;
    i=$(($i+1));
    ID=$(cat $FILE | head -n$i | tail -n1);
    sleep 10;
    curl -s 127.0.0.1:$PORT2/incentivecash+uid:$ID | jq '.response.uid','.response.details.lastPing'
    echo -e "Баланс: $(curl -s 127.0.0.1:$PORT2/incentivecash+uid:$ID | jq '.response.details.rewards.dailyRewards')"
    PORT1=$(($PORT1+2));
    PORT2=$(($PORT2+2));
  done
}

monitor() {
  curl -s 127.0.0.1:$i/incentivecash | jq '.response.uid','.response.details.lastPing'
  sleep 1
  echo -e "Баланс: $(curl -s 127.0.0.1:$i/incentivecash | jq '.response.details.rewards.dailyRewards')\n"
}

# setup
setup() {
  NEW="n"
  ADD="a"
  MON="m"
  echo "Что будем делать?"
  read VAR;
  VAR=$(echo "$VAR" | tr '[:upper:]' '[:lower:]')
  if [ "$VAR" = "$NEW" ]; then
    install
    # make configure new
    mkdir -p $HOME/.minima/
    nano $HOME/.minima/list_id && sed -i '/^$/d' $HOME/.minima/list_id
    FILE=$HOME/.minima/list_id
    PORT1=9001
    PORT2=9002
    run
  elif [ "$VAR" = "$ADD" ]; then
    # make configure add
    nano $HOME/.minima/list_id_add && sed -i '/^$/d' $HOME/.minima/list_id_add
    FILE=$HOME/.minima/list_id_add
    LASTPORT=$(docker ps | grep -Eo '90[0-9]+' | sort -nk 1 | tail -n 1);
    PORT1=$(($LASTPORT + 1))
    PORT2=$(($PORT1 + 1))
    run
    cat $HOME/.minima/list_id_add >> $HOME/.minima/list_id && rm -f $HOME/.minima/list_id_add
  elif [ "$VAR" = "$MON" ]; then
    PORTS=$(docker ps | grep -Eo '_9[0-9]+' | cut -c 2- | sort -nk 1)
    readarray -t arr <<< $PORTS;
    for i in "${arr[@]}";do
      if [ "null" != "$(curl -s 127.0.0.1:$i/incentivecash | jq '.response.details.rewards.dailyRewards')" ]; then
      monitor
      else
      echo -e ">>> Рестарт! Минутку..."
      docker restart minima_node_$i
      sleep 10
      echo "ОК! Перезапущен."
      monitor
      echo -e "=== === === === === === === === ==="
      echo -e "+ Посмотреть лог:\ndocker logs minima_node_$i\n"
      echo -e "+ Статус ноды:\ncurl -s 127.0.0.1:$i/status | jq\n"
      echo -e "+ Перезапуск контейнера:\ndocker rm -f minima_node_$i && rm -f $HOME/.minima/minima_$i && docker run -dit --restart on-failure --name minima_node_$i -v $HOME/.minima/minima_$i/:/minima/.minima -p $(($i-1)):9001 -p $i:9002 secord/minima;"
      echo -e "++ Обязательно зарегистрируйтесь в сети:\ncurl -s 127.0.0.1:$i/incentivecash+uid:<ВАШ_ID_NODE>"
      echo -e "=== === === === === === === === ===\n"
      fi
    done
  else
  echo "Введите предложенные варианты!"
  setup
  fi
}

#main
setup

echo "РАБОТА ЗАВЕРШЕНА"
